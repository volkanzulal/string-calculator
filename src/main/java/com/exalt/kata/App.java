package com.exalt.kata;

import java.util.Arrays;
import java.util.List;
import java.util.stream.*;



/**
 * String Calculator Kata
 * Author : Zulal Volkan
 */
public class App {
    
    private static int adder(String numbers, String delimiter) throws NumberFormatException{
        if(numbers.isEmpty()){
            return 0;
        }
        int sum = 0;
        String[] strTab = numbers.split(delimiter);
        List<String> negativeNumbers = Arrays.asList(strTab).stream().filter(e->e.startsWith("-")).collect(Collectors.toList());
        char[] sumTab = Arrays.stream(strTab)
        .collect(Collectors.joining())
        .toCharArray();

        if(!negativeNumbers.isEmpty()){
            throw new NumberFormatException("Negaive are not allowed : "+ negativeNumbers.toString());
        }
        // int ssum = Arrays.asList(sumTab).filter(char e -> Character.isDigit(e)).map(e->Character.getNumericValue(e)).collect(Collectors.summingInt(Integer::intValue));
        for(char e : sumTab){
            if(Character.isDigit(e)){
                sum += Character.getNumericValue(e);
            }
        }
        return sum;
    }

    public static int Add(String numbers) throws Exception{
        if(numbers.startsWith("//")){
            return adder(numbers,Character.toString(numbers.toCharArray()[2]));
        }
        return adder(numbers, ",");
    }

    public static void main( String[] args ) throws Exception{
        String str = " ";
        String str1 = "1\n2,3";
        String str2 = "//;\n1;2";
        String str3 = "1\n2,-3,-5,-8,-9";
        
        System.out.println(Add(str));
        System.out.println(Add(str1));
        System.out.println(Add(str2));
        System.out.println(Add(str3));
    }
}
