package com.exalt.kata;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    @Test
    public void testAddExceptionIfNegativeNumberArePresent() throws Exception
    {
        String str = "1\n2,-3,-5,-8,-19";
        NumberFormatException thrown =
        assertThrows(NumberFormatException.class,
           () -> App.Add(str),
           "Expected Add() to throw, but it didn't");

        assertTrue(thrown.getMessage().contains("Negaive are not allowed : [-3, -5, -8, -19]"));
    }

}
